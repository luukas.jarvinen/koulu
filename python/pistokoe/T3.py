vastaus = 0
while vastaus != 5:
    print("Mitä valitset: ")
    print("1 - Menen kahville")
    print("2 - Menen kotiin")
    print("3 - Menen kouluun")
    print("4 - Menen koodaamaan")
    print("5 - Menen pois")
    vastaus = int(input())
    if vastaus == 1:
            print("Ota kahvin kanssa myös kakkua :-)")
    if vastaus == 2:
        print("Hyvää kotimatkaa!")
    if vastaus == 3:
        print("Opiskele ahkerasti!")
    if vastaus == 4:
        print("Tervetuloa!")
    if vastaus == 5:
        print("Kiitos käynnistä!")
        break